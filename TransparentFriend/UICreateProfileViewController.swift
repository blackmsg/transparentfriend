//
//  ViewController.swift
//  TransparentFriend
//
//  Created by Vladimir Shapovalov on 19.07.16.
//  Copyright © 2016 vrsoft. All rights reserved.
//

import UIKit

class UICreateProfileViewController: UIViewController {
    
    public var createProfileInterface:CCreateProfile!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        // Объект интерфейса создания профиля 
        self.createProfileInterface = CCreateProfile(viewController: self)
        self.view.addSubview(self.createProfileInterface)        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // При нажатии на кнопку создать в правом верхнем углу
    @IBAction func onCreateProfile(sender: AnyObject) {
        GlobalData.AProfiles.append(self.createProfileInterface.getProfile())
        self.navigationController?.popToRootViewControllerAnimated(true)
    }

}