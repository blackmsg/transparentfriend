//
//  CLentaProfiles.swift
//  TransparentFriend
//
//  Предназначен для отображения списка всех активных профилей в ленте
//
//  Created by Vladimir Shapovalov on 28.07.16.
//  Copyright © 2016 vrsoft. All rights reserved.
//

import UIKit

class CLentaProfiles: UIView {
    
    public var viewController: UIViewController!
    
    override init (frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Инициализация
    convenience init (viewController: UIViewController) {
        self.init(frame: CGRect.zero)
        
        self.viewController = viewController
        
        for item in GlobalData.AProfiles {
            self.addSubview(CProfileItem(viewController: self.viewController, profile: item))
        }
    }
}
