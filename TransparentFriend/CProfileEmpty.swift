//
//  ProfileEmpty.swift
//  TransparentFriend
//
//  Предназначен для вывода сообщения "В ленте пусто, т.к. нет профиля"
//
//  Created by Vladimir Shapovalov on 25.07.16.
//  Copyright © 2016 vrsoft. All rights reserved.
//

import Foundation
import UIKit

class CProfileEmptry: UIView {
    public var OTitle: UITextView! // Текст сообщения
    public var OButton: UIButton! // Кнопка "Создать профиль"
    private let utils:Utils = Utils() // Вспомогательный объект нужен для заданиц цветов в hex
    public var viewController: UIRootViewController! // Нужен для центрации по центру экрана
    
    override init (frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init (viewController: UIRootViewController) {
        self.init(frame: CGRect.zero)
        
        self.viewController = viewController
        
        // Создать заголовок
        self.CreateTitle()
        // Создать кнопку
        self.CreateButton()
        
        // Перерисовка элементов
        self.Redraw()
        
        // Инициализация событий
        self.initEvents()       
    }
   
    
    // Инициализация событий
    private func initEvents() -> Void {
        // При повороте устройства
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CProfileEmptry.deviceRotated), name: UIDeviceOrientationDidChangeNotification, object: nil)
        
        // При нажатии на кнопку создать
        self.OButton.addTarget(self, action: #selector(CProfileEmptry.onButtonPress), forControlEvents: UIControlEvents.TouchDown)
    }
    
    // Нажатие на кнопку Создать
    func onButtonPress() -> Void {
        self.viewController.onCreateProfilePress()
    }
    
    // Поворот устройства
    func deviceRotated() -> Void {
        self.Redraw()
    }
    
    // Создание объекта заголовка
    private func CreateTitle() -> Void {
        self.OTitle = UITextView()
        
        self.OTitle.text = "В вашей ленте пусто\nт.к. нет профиля"
        self.OTitle.font = UIFont(name: "Arial", size: 18)
        self.OTitle.textAlignment = NSTextAlignment.Center
        self.OTitle.textColor = UIColor.blackColor()
        self.OTitle.backgroundColor = UIColor.clearColor()
        self.OTitle.editable = false
        self.OTitle.scrollEnabled = false
        
        self.addSubview(self.OTitle)
    }
    
    // Перерисовка заголовка
    public func RedrawTitle() -> Void {
        let contentSize: CGSize = self.OTitle.sizeThatFits(self.OTitle.bounds.size)
        let rect: CGRect = CGRect(x: 0, y: 0, width: contentSize.width, height: contentSize.height)
        
        self.OTitle.layer.frame = rect
        self.OTitle.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
    }
    
    // Создание кнопки
    private func CreateButton() -> Void {
        self.OButton = UIButton()
        
        self.OButton.setTitle("Создать", forState: UIControlState.Normal)
        self.OButton.backgroundColor = utils.getColor(0x26AAF9)
        self.OButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.OButton.layer.cornerRadius = 5
        
        self.addSubview(self.OButton)
    }
    
    // Перерисовка кнопки
    public func RedrawButton() -> Void {
        let contentSize: CGSize = (self.OButton.titleLabel?.sizeThatFits(self.OButton.titleLabel!.bounds.size))!
        let width = contentSize.width+40
        let height = contentSize.height+20
        self.OButton.frame.size.width = width
        self.OButton.frame.size.height = height
        self.OButton.frame.origin.x = (self.OTitle.frame.size.width - width)/2
        self.OButton.frame.origin.y = self.OTitle.frame.size.height + 5
    }
    
    // Отрисовка элементов
    public func Redraw() -> Void {
        self.RedrawTitle()
        self.RedrawButton()
        
        self.frame.size.width = self.OTitle.frame.size.width
        if(self.OTitle.frame.size.width < self.OButton.frame.size.width) {
            self.frame.size.width = self.OButton.frame.size.width
        }
        self.frame.size.height = self.OTitle.frame.size.height + 5 + self.OButton.frame.size.height
        
        // Центрация по центру экрана
        self.frame.origin.x = (self.viewController.view.frame.size.width - self.frame.size.width)/2
        self.frame.origin.y = (self.viewController.view.frame.size.height - self.frame.size.height)/2        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}