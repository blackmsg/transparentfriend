//
//  CreateProfile.swift
//  TransparentFriend
//
//  Предназначен для вывода элементов экрана "Создания профиля"
//
//  Created by Vladimir Shapovalov on 25.07.16.
//  Copyright © 2016 vrsoft. All rights reserved.
//

import Foundation
import UIKit

class CCreateProfile: UIView, UITextViewDelegate, UITextFieldDelegate {
    
    public var viewController: UICreateProfileViewController! // Родительский ViewController
    public var OPhotoLoader: PhotoLoader! // Объект отвечающий за изображения с камеры и библиотеки
    public var OProfileName: UITextField! // Поле ввода названия профиля
    private var OScrollView: UIScrollView! // Нужен для 
    
    private var KeyboardHeight: CGFloat = 0.0 // Высота клавиатуры
    
    private var navigationBarHeight: CGFloat = 0.0 // Высота навигационной панели
    private var infoBarHeight: CGFloat = 20.0 // Высота инфомации о времении и батареи
    
    override init (frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Инициализация
    convenience init (viewController: UICreateProfileViewController) {
        self.init(frame: CGRect.zero)
        
        self.viewController = viewController
        self.navigationBarHeight = (self.viewController.navigationController?.navigationBar.frame.size.height)!
        
        
        // Инициализация объекта для прокрутки
        self.OScrollView = UIScrollView()
        self.addSubview(self.OScrollView)
        
        // Создание объекта загрузки фотографий
        self.OPhotoLoader = PhotoLoader(viewController: self.viewController)
        
        // Добавление объекта загрузки фотографий в сцену
        self.OScrollView.addSubview(self.OPhotoLoader)        
        
        // Создание поля ввода имени профиля
        self.createProfileName()
        
        // События
        self.InitEvents()
        
        // Перерисовка
        self.redraw()
    }
    
    // Инициализация событий
    func InitEvents() -> Void {
        // При отображении клавиатуры
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CCreateProfile.KeyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        // При скрытии клавиатуры
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CCreateProfile.KeyboardWillShow(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    // Создание поля ввода имени профиля
    private func createProfileName() -> Void {
        self.OProfileName = UITextField()
        self.OProfileName.delegate = self
        
        self.OProfileName.returnKeyType = UIReturnKeyType.Done
        
        self.OProfileName.textColor = UIColor.blackColor()
        self.OProfileName.backgroundColor = Utils().getColor(0xefefef)
        
        self.OProfileName.placeholder = "Имя профиля"
        
        self.OScrollView.addSubview(self.OProfileName)
    }
    
    // Событие при нажатии на Done, Готово на клавиатуре
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        self.HideKeyboard()
        return true;
    }
    
    // Перерисовка поля ввода названия профиля
    public func redrawProfileName() -> Void {
        self.OProfileName.frame.size.width = self.viewController.view.frame.size.width - 40
        self.OProfileName.frame.size.height = 30
        self.OProfileName.frame.origin.x = 20
        self.OProfileName.frame.origin.y = self.OPhotoLoader.frame.origin.y + self.OPhotoLoader.frame.size.height + 20
    }
    
    // Изменение размеров объекта
    private func redraw() {
        // Перерисовка поля ввода названия профиля
        redrawProfileName()
        
        let width = self.viewController.view.frame.size.width
        let height = self.viewController.view.frame.size.height - self.KeyboardHeight
        
        
        self.OScrollView.frame.origin.x = 0
        self.OScrollView.frame.origin.y = 0
        self.OScrollView.frame.size.width = width
        self.OScrollView.frame.size.height = height
        self.OScrollView.contentSize.width = width
        self.OScrollView.contentSize.height = self.OProfileName.frame.origin.y + self.OProfileName.frame.size.height
        self.frame.size.width = self.OPhotoLoader.frame.size.width
        self.frame.size.height = height
    }
    
    // Cкрыть клавиатуру
    func HideKeyboard() -> Void {
        endEditing(true)
    }
    
    // При показе клавиатуры
    func KeyboardWillShow(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardHeight:CGFloat = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue().size.height
    
    
        if( notification.name == UIKeyboardWillShowNotification ) {
            // Клавиатура отображена
            self.KeyboardHeight = keyboardHeight
        } else {
            // Клавиатура скрыта
            self.KeyboardHeight = 0
        }
       
        
        // Перерисовка элементов
        self.redraw()
        
        
        let y = -self.navigationBarHeight-self.infoBarHeight + self.OProfileName.frame.size.height
        if(self.OProfileName.frame.origin.y + self.OProfileName.frame.size.height < self.OScrollView.contentSize.height) {
            self.OScrollView.setContentOffset(CGPoint(x: 0, y: y), animated: true)
        }
        
    }
    
    // Возвращает объект профиля для сохранения в глобальные данные
    func getProfile() -> CProfile {
        return CProfile(name: self.OProfileName.text!, image: self.OPhotoLoader.OImageView.image!)
    } 
    
}
