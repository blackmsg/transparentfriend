//
//  ViewController.swift
//  TransparentFriend
//
//  Created by Vladimir Shapovalov on 19.07.16.
//  Copyright © 2016 vrsoft. All rights reserved.
//

import UIKit

class UIRootViewController: UIViewController {
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    private var profileEmpty: CProfileEmptry!
    private var lentaProfiles: CLentaProfiles!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Привязка события показа бокового меню к иконке меню в верхнем левом углу
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = "revealToggle:"
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Событие при нажатии на кнопку "Создать" профиль
    public func onCreateProfilePress() -> Void {
        let CreateProfileScr = self.storyboard?.instantiateViewControllerWithIdentifier("CreateProfileScr") as! UICreateProfileViewController // Экран создания профиля
        
        // Переход к экрану создания профиля
        self.navigationController?.pushViewController(CreateProfileScr, animated: true)
    }
    
    // При возвращении к экрану
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if( GlobalData.AProfiles.count == 0 ) { // Если не созданное ни одного профиля
            // Сообщение о том, что нет профилей
            self.profileEmpty = CProfileEmptry(viewController: self)
            self.view.addSubview(self.profileEmpty)
        } else {
            // Если созданы профили убрать сообщение о том, что нечего не созданно
            if( self.profileEmpty != nil ) {
                if( self.profileEmpty.superview != nil ) {
                    self.profileEmpty.removeFromSuperview()
                }
            }
            // Отобразить профили пользователей включая свой
            self.lentaProfiles = CLentaProfiles(viewController: self)
            self.view.addSubview(self.lentaProfiles)
        }
        
    }


}

