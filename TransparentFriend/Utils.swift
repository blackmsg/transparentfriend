//
//  Utils.swift
//  TransparentFriend
//
//  Created by Vladimir Shapovalov on 25.07.16.
//  Copyright © 2016 vrsoft. All rights reserved.
//

import Foundation
import UIKit

class Utils: NSObject {
    internal func getColor(hex: Int) -> UIColor {
        return UIColor(netHex: hex)
    }
    
    internal func getInt(a: Int) -> Int {
        return a;
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}