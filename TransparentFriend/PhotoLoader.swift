//
//  CreateProfile.swift
//  TransparentFriend
//
//  Предназначен отображения и загрузки фотографии с камеры или библиотеки
//
//  Created by Vladimir Shapovalov on 27.07.16.
//  Copyright © 2016 vrsoft. All rights reserved.
//

import Foundation
import UIKit

class PhotoLoader: UIView, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    public var OImageView: UIImageView! // Область для изображения
    public var OAddButton: UIButton! // Кнопка добавить изображение профиля
    public var imagePicker: UIImagePickerController! // Объект для выбора изображений из библиотеки или камеры
    
    public var viewController: UIViewController! // Родительский ViewController
    
    private let utils: Utils = Utils() // Для конвертации цвета из hex
    
    override init (frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Инициализация
    convenience init (viewController: UIViewController) {
        self.init(frame: CGRect.zero)
        
        self.viewController = viewController
        
        // Создать изображение для фотографии
        self.createImageView()
        
        // Создание кнопки + Добавить изображение профиля
        self.createButton()
        
        // Отрисовка элементов
        self.redraw()
        
        
        // События
        self.initEvents()
        
    }
    
    // Создание изображения для фото
    private func createImageView() -> Void {
        self.OImageView = UIImageView()
        self.OImageView.backgroundColor = utils.getColor(0xefefef)
        self.OImageView.contentMode = UIViewContentMode.Center
        self.OImageView.image = UIImage(named: "NoPhoto")
        
        self.addSubview(self.OImageView)
    }
    
    // Отрисовка изображения для фото
    public func redrawImageView() -> Void {
        let w: CGFloat = self.viewController.view.frame.size.width
        let h: CGFloat = 250
        let y: CGFloat = 0//(self.viewController.navigationController?.navigationBar.frame.size.height)!
        self.OImageView.frame = CGRect(x: 0, y: y, width: w, height: h)
    }
    
    // Создание кнопки "+ Добавить изображение профиля"
    private func createButton() -> Void {
        self.OAddButton = UIButton()
        
        self.OAddButton.setTitle("+ Добавить изображение профиля", forState: .Normal)
        self.OAddButton.setTitleColor(utils.getColor(0x26AAF9), forState: .Normal)
        
        self.addSubview(self.OAddButton)
    }
    
    // Отрисовка кнопки
    public func readrawAddButton() -> Void {
        let contentSize: CGSize = (self.OAddButton.titleLabel?.sizeThatFits(self.OAddButton.titleLabel!.bounds.size))!
        
        self.OAddButton.frame.size.width = contentSize.width + 10
        self.OAddButton.frame.size.height = contentSize.height + 10
        
        let x = (self.OImageView.frame.size.width - self.OAddButton.frame.size.width) / 2
        let y = (self.OImageView.frame.origin.y + self.OImageView.frame.size.height + 10)
        self.OAddButton.frame.origin.x = x
        self.OAddButton.frame.origin.y = y
    }
    
    // Отрисовка всех элементов
    public func redraw() {
        // Отрисовка изображения
        self.redrawImageView()
        
        // Отрисовка кнопки
        self.readrawAddButton()
        
        self.frame.size.width = self.viewController.view.frame.size.width
        self.frame.size.height = self.OAddButton.frame.origin.y + self.OAddButton.frame.size.height
    }
    
    // Инициализация событий
    private func initEvents() -> Void {
        // Нажатие на кнопку "+ Добавить изображение профиля"
        self.OAddButton.addTarget(self, action: #selector(PhotoLoader.showMenu), forControlEvents: UIControlEvents.TouchDown)
    }
    
    // Сделать фото
    public func takePhoto() -> Void {
        self.imagePicker = UIImagePickerController()
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = .Camera
        
        self.viewController.presentViewController(self.imagePicker, animated: true, completion: nil)
    }
    
    // Показать библиотеку
    public func showLibrary() -> Void {
        self.imagePicker = UIImagePickerController()
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = .PhotoLibrary
        
        self.viewController.presentViewController(self.imagePicker, animated: true, completion: nil)
    }
    
    // Удалить изображение профиля
    public func deleteImage() -> Void {
        self.OImageView.contentMode = UIViewContentMode.Center
        self.OImageView.image = UIImage(named: "NoPhoto")
    }
    
    // Отображение меню при нажатии на фотографию
    func showMenu() -> Void {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .Cancel) { (action) in
            // ...
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "Сделать фото", style: .Default) { (action) in
            // Сделать фото
            self.takePhoto()
        }
        alertController.addAction(OKAction)
        
        let LibraryAction = UIAlertAction(title: "Библиотека", style: .Default) { (action) in
            // Показать библиотеку
            self.showLibrary()
        }
        alertController.addAction(LibraryAction)
        
        let DeleteAction = UIAlertAction(title: "Удалить", style: .Destructive) { (action) in
            // Удалить изображение профиля
            self.deleteImage()
        }
        alertController.addAction(DeleteAction)
        
        self.viewController.presentViewController(alertController, animated: true) {
            // ...
        }
    }
    
    // Собитие при съемке или выборе фотографии из библиотеки
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        self.OImageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.OImageView.contentMode = UIViewContentMode.ScaleAspectFit
        self.OImageView.clipsToBounds = true
        self.viewController.dismissViewControllerAnimated(true, completion: nil)
    }
}