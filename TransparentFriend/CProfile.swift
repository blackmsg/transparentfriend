//
//  CProfile.swift
//  TransparentFriend
//
//  Created by Vladimir Shapovalov on 28.07.16.
//  Copyright © 2016 vrsoft. All rights reserved.
//

import UIKit

class CProfile {
    public var name: String = ""
    public var image: UIImage!
    
    init (name: String, image: UIImage) {
        self.name = name
        self.image = image
    }
}
