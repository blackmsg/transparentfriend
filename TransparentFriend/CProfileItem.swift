//
//  CProfileItem.swift
//  TransparentFriend
//
//  Класс для отображения единичного профиля
//
//  Created by Vladimir Shapovalov on 28.07.16.
//  Copyright © 2016 vrsoft. All rights reserved.
//

import UIKit

class CProfileItem: UIView {
    public var viewController: UIViewController! // Корневой ViewController
    public var OProfile: CProfile! // Объект информации о профиле
    public var OImageView: UIImageView! // Объект фотографии профиля
    public var OLabel: UILabel! // Объект названия профиля
    
    override init (frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Инициализация
    convenience init (viewController: UIViewController, profile: CProfile) {
        self.init(frame: CGRect.zero)
        
        self.viewController = viewController
        self.OProfile = profile

        // Создание элементов
        self.createItems()
        
        // Отрисовка элементов
        self.redraw()
    }
    
    private func createItems() -> Void {
        // Изображение
        self.OImageView = UIImageView()
        self.OImageView.backgroundColor = Utils().getColor(0xefefef)
        self.OImageView.contentMode = UIViewContentMode.ScaleAspectFit
        self.OImageView.image = OProfile.image
        self.addSubview(self.OImageView)
        
        // Название
        self.OLabel = UILabel()
        self.OLabel.text = OProfile.name
        self.OLabel.textColor = UIColor.blackColor()
        self.OLabel.backgroundColor = UIColor.whiteColor()
        self.OLabel.textAlignment = NSTextAlignment.Center
        self.addSubview(self.OLabel)
    }
    
    // Отрисовка
    public func redraw() {
        // Отрисовка изображения
        self.OImageView.frame.origin.x = 0
        self.OImageView.frame.origin.y = 0
        self.OImageView.frame.size.width = self.viewController.view.frame.size.width
        self.OImageView.frame.size.height = 200
        
        // Отрисовка названия
        let contentSize = self.OLabel.sizeThatFits(self.OLabel.bounds.size)
        let x = (self.OImageView.frame.size.width - contentSize.width)/2
        self.OLabel.frame = CGRect(x: x, y: self.OImageView.frame.size.height - contentSize.height, width: contentSize.width + 10, height: contentSize.height)
    }
}
